var product_amount = document.getElementById('product_total_amo');
var shipping_charge = document.getElementById('shipping_charge');
var total_cart_amt = document.getElementById('total_cart_amt');

var discount_code1 = document.getElementById('discount_code1');

const decreaseNumber = (incdec,item_price) =>{
    var itemval = document.getElementById(incdec);
    var itemprice = document.getElementById(item_price);

    if(itemval.value <= 0){
        itemval.value = 0;
    }else{
        itemval.value = parseInt(itemval.value) - 1;
        itemval.style.background = '#fff';
        itemval.style.color = '#000';
        itemprice.innerHTML = parseInt(itemprice.innerHTML) - 15;
        product_amount.innerHTML = parseInt(product_amount.innerHTML) - 15;
        total_cart_amt.innerHTML = parseInt(product_amount.innerHTML) + parseInt(shipping_charge.innerHTML);
    }
}

const increaseNumber = (incdec,item_price) =>{
    var itemval = document.getElementById(incdec);
    var itemprice = document.getElementById(item_price);
    
    if(itemval.value >= 5){
        itemval.value = 5;
        alert('max 5 allowed');
        itemval.style.background = 'red';
        itemval.style.color = '#fff';
    }else{
        itemval.value = parseInt(itemval.value) + 1;
        itemprice.innerHTML = parseInt(itemprice.innerHTML) + 15;
        product_amount.innerHTML = parseInt(product_amount.innerHTML) + 15;
        total_cart_amt.innerHTML = parseInt(product_amount.innerHTML) + parseInt(shipping_charge.innerHTML);
    }
}

const discount_code = () => {
    let totalamount = parseInt(total_cart_amt.innerHTML);
    if(discount_code1.value === 'rifat'){
        let newtotal = totalamount - 15;
        total_cart_amt.innerHTML = newtotal;
    }else{
        alert('Try again! Code is not valid');
    }
}